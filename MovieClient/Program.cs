﻿using System;
using Grpc.Net.Client;
using Google.Protobuf;
using System.Threading.Tasks;
using MovieService;

namespace GrpcMovieClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new MovieFounder.MovieFounderClient(channel);
            var reply = await client.FindAsync(
                              new MovieRequest { Name = "Lord" });
            Console.WriteLine(reply.Message);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
