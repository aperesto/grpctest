using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace MovieService
{
    public class MovieFounderService : MovieFounder.MovieFounderBase
    {
        private readonly ILogger<MovieFounderService> _logger;
        public MovieFounderService(ILogger<MovieFounderService> logger)
        {
            _logger = logger;
        }

        public override Task<MovieReply> Find(MovieRequest request, ServerCallContext context)
        {
            string jsonResponse;
            string url = @"http://omdbapi.com/?apikey=33e80834&t={0}";

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(string.Format(url, request.Name));
            req.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)req.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                jsonResponse = reader.ReadToEnd();
            }

            return Task.FromResult(new MovieReply
            {

                Message = jsonResponse
            });
        }
    }
}
