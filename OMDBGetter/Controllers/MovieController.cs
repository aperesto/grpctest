﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace OMDBGetter.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MovieController : ControllerBase
    {
        private readonly ILogger<MovieController> _logger;

        public MovieController(ILogger<MovieController> logger)
        {
            _logger = logger;
        }

        [HttpGet("/movie/{title}")]
        public Movie FindMovieByTitle(string title)
        {

            string jsonResponse;
            string url = @"http://omdbapi.com/?apikey=33e80834&t={0}";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format(url, title));
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                jsonResponse = reader.ReadToEnd();
            }

            var jsonOptions = new JsonSerializerOptions
            {
                AllowTrailingCommas = true,
            };

            return JsonSerializer.Deserialize<Movie>(jsonResponse, jsonOptions);
        }

    }
}
